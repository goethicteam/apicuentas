'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var AccountSchema = new Schema({
  id: {
    type: String,
  },
  alias: {
    type: String,
    Required: 'Client’s name'
  },
  type: {
    type: String,
    Required: 'Client’s surname'
  },
  description: {
    type: String,
    Required: 'Client’s second surname'
  },
  number: {
    type: String,
    Required: 'Client’s gender values are: MALE or FEMALE'
  },
  userId: {
    type: Number
  },
  balance: {
    type: Number,
    Required: 'Client’s birth date'
  },
  currency: {
    type: String,
    Required: 'Client’s identity'
  },
  accountTransactions:[
    {
      id: {
        type: String
      },
      amount: {
        type: Number,
        Required: 'Client’s name'
      },
      currency: {
        type: String,
        Required: 'Client’s surname'
      },
      description: {
        type: String,
        Required: 'Client’s second surname'
      },
      operationDate: {
        type: Date,
        Required: 'Client’s second surname'
      },
      valueDate: {
        type: Date,
        Required: 'Client’s second surname'
      },
      category: {
        id: {
          type: Number
        },
        name: {
          type: String
        }
      },
      subCategory: {
        id: {
          type: Number
        },
        name: {
          type: String
        }
      }
    }
  ]
});

module.exports = mongoose.model('Account', AccountSchema);
