'use strict';

var mongoose = require('mongoose'),
  account = mongoose.model('Account');


exports.list_all_accounts = function(req, res) {
  account.find({userId: req.get('UserID')}, function(err, account) {

    if (err)
      res.send(err);
    res.json(account);
  });
};


exports.create_a_account = function(req, res) {
  var new_account = new account(req.body);
  new_account.save(function(err, account) {
    if (err)
      res.send(err);
    res.json(account);
  });
};

exports.read_a_account = function(req, res) {
  account.find({id : req.params.id}, function(err, account) {

    if (err)
      res.send(err);
    res.json(account);
  });
};

exports.update_a_account = function(req, res) {
  account.findOneAndUpdate({id:req.params.id}, req.body, {new: true}, function(err, account) {
    if (err)
      res.send(err);
    res.json(account);
  });
};

exports.add_a_transacction = function(req,res) {
  account.update({id:req.params.id}, {'$addToSet':{"accountTransactions":req.body}}, function(err, account){
    if (err)
      res.send(err);
    res.json(account);
  });
};

exports.delete_a_account = function(req, res) {
  account.remove({
    id: req.params.id
  }, function(err, account) {
    if (err)
      res.send(err);
    res.json({ message: 'account successfully deleted' });
  });
};
