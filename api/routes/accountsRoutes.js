'use strict';

module.exports = function(app) {
	var customers = require('../controllers/accountsController');

	// todoList Routes
	app.route('/accounts')
		.get(customers.list_all_accounts)
		.post(customers.create_a_account);

	app.route('/accounts/:id')
		.get(customers.read_a_account)
		.patch(customers.add_a_transacction)
		.put(customers.update_a_account)
		.delete(customers.delete_a_account);

};
